import numpy as np
import math
import torch
import weibull as weibull

def cosine(x, y):
    x_norm = torch.linalg.vector_norm(x, dim=1)
    y_norm = torch.linalg.vector_norm(y, dim=1)
    dot = torch.mm(x / x_norm[:, None], y.T / y_norm[None, :])
    return 1 - dot
    
def euclidean(x, y):
    return torch.cdist(x, y, p=2.0, compute_mode='donot_use_mm_for_euclid_dist')

def gpu_torch_distances(data, device, batch_size, metric):
    data = data.to(torch.float32).to(device)
    all_distances = []
    for i in range((len(data) + batch_size - 1) // batch_size):
        batch = data[i * batch_size:(i + 1) * batch_size]
        if metric == "cosine":
            batch_distances = cosine(batch, data)
        elif metric == "euclidean":
            batch_distances = euclidean(batch, data)
        all_distances.append(batch_distances)
    return torch.cat(all_distances).fill_diagonal_(-torch.inf).cpu().to(torch.float32)

def tolerance(features, device, metric, batch_size):
    distances = gpu_torch_distances(features, device, batch_size, metric)
    return compute_tau(distances, features, device, metric, batch_size) + tuple([distances])

def compute_tau(distances, features, device, metric, batch_size):
    if distances.numel() < 10000:
        average_distance = torch.median(distances)
    else:
        average_distance = torch.median(torch.flatten(distances)[[torch.randint(high=distances.numel(), size=(10000,))]])
    max_distance = torch.max(distances)
    nearest_points = torch.flatten(distances.topk(2, largest=False).indices[:, 1])
    nearest_points_distances = distances[[list(range(len(nearest_points))), nearest_points]]
    nearest_points_indices = torch.argsort(nearest_points_distances)

    nearest_cluster_with_distance_round_1 = [
        (nearest_points_distances[i].item(), (i, nearest_points[i].item())) for i in map(lambda x: x.item(), nearest_points_indices)
    ]

    # generate order
    order = np.argsort(-np.array(list(set(nearest_points))))

    # add non kernel points to order
    processed = set()
    init0 = []
    init1 = []
    for i in order:
        if i in processed:
            continue

        j = nearest_points[i]
        if j in processed:
            continue

        init0.append(i)
        init1.append(j)
        processed.add(i)
        processed.add(j)
    init_length = len(init0)
    init0 = torch.tensor(init0)
    init1 = torch.tensor(init1)

    centroids = (features[[init0]] + features[[init1]]) / 2
    dist = gpu_torch_distances(centroids, device, batch_size, metric)
    nearest_init = torch.flatten(dist.topk(2, largest=False).indices[:, 1])

    centroids0 = centroids
    centroids1 = (features[[init0[nearest_init]]] + features[[init1[nearest_init]]]) / 2

    x = centroids0.to(device)
    y = centroids1.to(device)
    if metric == "cosine":
        x_norm = torch.linalg.vector_norm(x, dim=1)
        y_norm = torch.linalg.vector_norm(y, dim=1)
        gxs = 1 - torch.linalg.vecdot(x / x_norm[:, None], y / y_norm[:, None], dim=1)
    elif metric == "euclidean":
        gxs = euclidean(x, y).diagonal()
    gxs = gxs.cpu()

    tau = torch.max(gxs).item() * average_distance / max_distance
    return tau.item(), nearest_points, init_length, [nearest_cluster_with_distance_round_1[i] for i in order], nearest_points_distances

    
def nan_to_num(t,mynan=0.):
    if torch.all(torch.isfinite(t)):
        return t
    if len(t.size()) == 0:
        return torch.tensor(mynan)
    return torch.cat([nan_to_num(l).unsqueeze(0) for l in t],0)

################################################################
##################EVT-VERSION###################################
################################################################
def get_tau(data,maxval,name,tailfrac=1,pcent=.99,usehigh=True,maxmodeerror=.05):
    tw =  weibull.weibull()
    tau = -1
    while(tau < 0):
      nbin=100
      nscale = 10
      fullrange = torch.linspace(0,maxval,nbin)
      fsize = max(3,int(tailfrac*len(data)))    
      if(usehigh):
          tw.FitHighTrimmed(data.view(1,-1),fsize)
      else:
          tw.FitLowReversed(data.view(1,-1),fsize)
      parms = tw.return_all_parameters()
      if(usehigh):
          tau=  parms['Scale']*np.power(-np.log((1-pcent)),(1/parms['Shape'])) - parms['translateAmountTensor'] + parms['smallScoreTensor']
      else:
          tau = parms['translateAmountTensor']- parms['smallScoreTensor']-(parms['Scale']*np.power(-np.log((pcent)),(1/parms['Shape'])))
      if(math.isnan(tau)):
          print( name , "Parms", parms)        
          tau = torch.mean(data)
      wmode = float(parms['translateAmountTensor']- parms['smallScoreTensor']+ (parms['Scale']*np.power((parms['Shape']-1)/(parms['Shape']),1./parms['Shape']          )))

      wscoresj = tw.wscore(fullrange)
      probj = nan_to_num(tw.prob(fullrange))
      if(torch.sum(probj) > .001):
          probj = probj/torch.sum(probj)
      datavect=data.numpy()
      histc,hbins = np.histogram(datavect,bins=nbin,range=[0,1])
      imode = hbins[np.argmax(histc[0:int(tau*nbin+1)])]
      merror = abs(imode-wmode)
      if(merror > maxmodeerror):
          #outlier detected, reduce tail fraion and force loop
          tailfrac = tailfrac - .05
          tau = -1
    print(name," EVT Tau with data fraction ", round(tailfrac*100, 2)," Percentile ",pcent*100,"   is ", float(tau.numpy()))
    return tau.numpy()

def nan_to_num1(t,mynan=0.):
    if torch.all(torch.isfinite(t)):
        return t
    if len(t.size()) == 0:
        return torch.tensor(mynan)
    return torch.cat([nan_to_num(l).unsqueeze(0) for l in t],0)


def get_tau1(data,maxval,tailfrac=.25,pcent=.999):
    #tw =  weibull.weibull(translateAmountTensor=.001)
    tw = weibull.weibull()
    nbin=200
    nscale = 10
    #fullrange = torch.linspace(0,torch.max(ijbbdata),nbin)
    fullrange = torch.linspace(0,maxval,nbin)
    torch.Tensor.ndim = property(lambda self: len(self.shape))
    #print( name , "Data mean, max", torch.mean(ijbbdata),torch.max(ijbbdata))
    imean = torch.mean(data)
    istd = torch.std(data)
    imax = torch.max(data)
    tw.FitHighTrimmed(data.view(1,-1),int(tailfrac*len(data)))
    parms = tw.return_all_parameters()
    wscoresj = tw.wscore(fullrange)
    probj = nan_to_num(tw.prob(fullrange))
    if(torch.sum(probj) > .001):
        probj = probj/torch.sum(probj)
    tau=  parms['Scale']*np.power(-np.log((1-pcent)),(1/parms['Shape'])) - parms['translateAmountTensor'] + parms['smallScoreTensor']        
    return tau.numpy()

def thread(threads):
    for t in threads:
        t.setDaemon(True)
        t.start()
    for t in threads:
        t.join()

def takeSecond(elem):
    return elem[1]
