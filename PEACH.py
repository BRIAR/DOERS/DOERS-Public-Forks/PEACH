import numpy as np
import scipy
import torch
import torch.nn as nn
#from pyflann import *
from tau_flann_pytorch import tolerance, gpu_torch_distances
from merge import merging, merging_combine_sum
from evaluate import convert_clusters_to_label
#from clustering import KNN

def cosine(x, y):
    x = nn.functional.normalize(x, dim=1)
    y = nn.functional.normalize(y, dim=1)
    similarity = torch.einsum('nc,ck->nk', [x, y.T])
    distances = 1 - similarity
    return distances

def euclidean(x, y):
    distances = torch.cdist(x, y, p=2.0, compute_mode='donot_use_mm_for_euclid_dist')
    return distances

def PEACH(features: torch.Tensor, device: torch.device, metric="cosine", batch_size=4096, no_singleton=False):
    if len(features) == 0:
        return np.array([], dtype=np.int32)
    if len(features) == 1:
        return np.array([0], dtype=np.int32)

    estimated_gap, _, _, nearest_cluster_with_distance_round_1, _, distances = tolerance(features, device, metric, batch_size)

    length = len(features)
    clusters = [[i] for i in range(length)]
    cluster_centroids = {i:(features[i], 1) for i in range(length)}

    round = 0
    while True:
        if len(clusters) < 2:
            break

        if round == 0:
            # In round 1 the centroids is the points no matter what's linkage
            # Merging by nearest points
            nearest_cluster_with_distance = nearest_cluster_with_distance_round_1
            nearest_cluster = []
            nearest_cluster_dis = []
            for m in sorted(nearest_cluster_with_distance, key=takeSecond):
                nearest_cluster_dis.append(m[0])
                nearest_cluster.append(m[1][1])
            #############################################
            ###############Generate merging list#########
            #############################################
            merging_list = set()
            merging_list_with_cluster_id = set()
            processed = set()
            # Generate merging list (cluster pairs to be merged)(sorted by shortest distance)
            for i, j in enumerate(nearest_cluster_with_distance):
                if j[1][0] not in processed and j[1][1] not in processed:
                    merging_list.add(tuple(j[1]))
                    merging_list_with_cluster_id.add((tuple(clusters[j[1][0]]), tuple(clusters[j[1][1]])))
                    processed.add(j[1][0])
                    processed.add(j[1][1])

        else:
            centroids = torch.stack([cluster_centroids[cluster[0]][0] for cluster in clusters])

            distances = gpu_torch_distances(centroids, device, batch_size, metric)
            nearest_clusters = torch.flatten(distances.topk(2, largest=False).indices[:, 1])
            nearest_clusters_distances = torch.tensor([distances[i, j] for i, j in enumerate(nearest_clusters)])
            nearest_clusters_indices = torch.argsort(nearest_clusters_distances)

            nearest_cluster_with_distance = [(nearest_clusters_distances[i].item(), (i, nearest_clusters[i].item())) for i in map(lambda x: x.item(), nearest_clusters_indices)]

            #############################################
            ###############Generate merging list#########
            #############################################
            merging_list = set()
            merging_list_with_cluster_id = set()
            processed = set()
            # Generate merging list (cluster pairs to be merged)(sorted by shortest distance)
            for i, j in enumerate(nearest_cluster_with_distance):
                if j[1][0] not in processed and j[1][1] not in processed:
                    merging_list.add(tuple(j[1]))
                    merging_list_with_cluster_id.add((tuple(clusters[j[1][0]]), tuple(clusters[j[1][1]])))
                    processed.add(j[1][0])
                    processed.add(j[1][1])

            # Select by shorest distance. Each cluster only merge once per round.
            # example: sorted nearest_cluster_label [[0, 5], [5, 0], [2, 3], [3, 2], [4, 5], [1, 3]] then we got merging_list [[0, 5], [2, 3]]
            # Now let's decide to merge cluster0 and cluster1 from merging list

        #############################################
        ###############Start merging#################
        #############################################
        clusters = merging(merging_list, clusters, cluster_centroids, distances, estimated_gap, metric)
        # rembember old and new clusters
        result_clusters = [k for k in clusters if len(k) != 0]
        #########################################################################################
        if len(clusters) == len(result_clusters):  # Break if there is no new clusters found (nothing to merge).
            break
        clusters = result_clusters
        round += 1
    # clusters = result_clusters

    if no_singleton == True:
        true_clusters = [i for i in result_clusters if len(i) != 1]
        singletons = [k for k in clusters if len(k) == 1]
        centroids = [np.mean(cluster) for cluster in true_clusters]
        for single in singletons:
            dis = [scipy.spatial.distance.cosine(single, centroid) for centroid_id, centroid in enumerate(centroids)]
            index = np.argsort(dis)[0]
            true_clusters[index].extend(single)
        clusters = true_clusters

    labels = np.array(convert_clusters_to_label(clusters, length))
    return labels



def takeFirst(elem):
    return elem[0]

def takeSecond(elem):
    return elem[1]

def takeThird(elem):
    return elem[2]

def takeFourth(elem):
    return elem[3]

def thread(threads):
    for t in threads:
        t.setDaemon(True)
        t.start()
    for t in threads:
        t.join()


if __name__ == '__main__':
    main()
